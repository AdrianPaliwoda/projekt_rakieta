Celem projektu było stworzenie w języku Java biblioteki zawierającej funkcje, które pozwolą na obliczenie zależności fizycznych i oddziaływań, jakie maja miejsce podczas otu rakiety i innych obiektów.
Funkcje jakie zostały uznane za ważne to:

- Sprawdzenie czy dla danej prędkości rakieta osiągnie orbitę?
- Sprawdzenie czy rakieta opuści pole grawitacyjne planety?
- Sprawdzenie czy rakieta opuści układ planetarny?
- Sprawdzenie czy rakieta opuści galaktykę?
- Po jakim czasie rakieta osiągnie prędkość kosmiczna dla podanego przyspieszenia?
- Sprawdzenie, jakie przyspieszenie jest potrzebne by rakieta osiągnęła prędkość kosmiczna w podanym czasie

Jednak, aby je zrealizować stworzono funkcje(metody) pomocniczy zajmujące się:

- 1 Prędkością Kosmiczna
- 2 Prędkością Kosmiczna
- 3 Prędkością Kosmiczna
- Okresem w ruchu obrotowym
- 3 zasadami dynamiki Newtona
- Przyśpieszeniem grawitacyjne
- Ruchem opóźniony
- Ruchem przyśpieszony
- Ruchem obrotowy

Biblioteka składa się z 2 klas, w których znajdują się metody dla użytkownika. Zostały podzielone na 2, aby łatwiej było z nich korzystać. Pierwsza z nich – Space zawiera funkcje ważne wymienione na początku dokumentu, druga – Physics – zawiera mniejsze funkcje, z których korzysta Space, ale nie tylko, zawiera ona także funkcjonalności związane nie tylko z kosmosem.
Funkcje jakie udało nam się wykonać dla klasy Space:

- ifRacketStayOnOrbit(Planet planet, Racket racket)
- ifRacketEscapeGravityField(Planet planet, Racket racket)
- boolean ifRacketEscapeSystem(Star star, Racket racket)
- ifRacketEscapeGalaxy(Galaxy galaxy, Racket racket)
- timeToOrbitalSpeed(Racket racket, Planet planet)
- accelerationToOrbitalSpeed(Racket racket, Planet planet, Double time)

Funkcje jakie zostały wykonane dla klasy Physics:

- orbitalSpeed(Double gravitationalConstant, Double planetMass, Double radius)
- escapeVelocity(Double gravitationalConstant, Double planetMass, Double radiusPlanet)
- escapeVelocityFromSystem(Double gravitationalConstant, Double starMass, Double radius)
- orbitalPeriod(Double velocity, Double radius)
- accelerationNewton(Double force, Double mass
- velocityFreeFall(Double gravity, Double time)
- heightFreeFall(Double gravity, Double time)
- momentum(Double velocity, Double mass)
- acceleration(Double velocityBegin, Double velocityEnd, Double deltaTime) acceleration(Double deltaVelocity, Double deltaTime)
- acceleration(Double velocityBegin, Double velocityEnd, Double timeBegin, Double timeEnd)
- velocityEnd(Double velocityBegin, Double acceleration, Double deltaTime)
- distance(Double velocityBegin, Double acceleration, Double deltaTime)
- velocityLineToAngular(Double velocityLine, Double radius)
- velocityAngularToLine(Double velocityAngular, Double radius)
- angularVelocity(Double deltaAngular, Double deltaTime)
- angular(Double angularLenght, Double radius)
- Angular, Double radius)
- timeToVelocityEnd(Double velocityBegin, Double velocityEnd, Double acceleration)
